########################
# Starting with Ubuntu SSH (see other directory for docker file.)
FROM hharmon/nanite:0.0

# Pull in the nanite pre-compiled library
COPY nanite-0.0.1-Linux.deb /
RUN  apt-get install /nanite-0.0.1-Linux.deb
COPY *.json /
COPY *.htm /
CMD [ "nanite", "/modbus_server.json" ]
